# include "DFRobot_LedDisplayModule.h"

#define SWROT1 2
#define SWROT2 3
#define SWROT3 4
#define SWROT4 5
#define SWROT5 6
#define SWROT6 7

#define PRSSR A1

#define R1 4700.0
#define R2 4700.0

#define PA 100;
#define MTORR 7.5;
#define HPA 1;
#define KPA 0.1;
#define TORR 0.0075;
#define BAR 0.001;

DFRobot_LedDisplayModule LED(&Wire, 0xE0);

float read_pressure(float unit, uint8_t samples, uint8_t wait) {
  float avg = 0;
  for(uint8_t i = 0; i<samples; i++) {
    avg += analogRead(PRSSR);
    delay(wait);
  }
  avg /= (float)samples;
  float voltage = avg * (R1 + R2) * 5.0 / R2 / 1024.0;
  float pressure = pow(10, (voltage - 7.75)/0.75) * unit;
  return pressure;
}

float rotsw_to_unit() {
  if(!digitalRead(SWROT6)) {
    return BAR;
  } else if(!digitalRead(SWROT5)) {
    return TORR;
  } else if(!digitalRead(SWROT4)) {
    return KPA;
  } else if(!digitalRead(SWROT3)) {
    return HPA;
  } else if(!digitalRead(SWROT2)) {
    return MTORR;
  } else if(!digitalRead(SWROT1)) {
    return PA;
  }
}

void float_to_exp(float num, float *mant, int8_t *exp) {
  *exp = 0;
  *mant = num;
  if(num == 0) {
    *mant = 0;
    *exp = 1;
    return;
  }
  if(abs(num) > 1) {
    while(*mant >= 10) {
      *mant /= 10.0;
      *exp+=1;
    }
  } else {
    while(fabs(*mant) < 1) {
      *mant *= 10.0;
      *exp-=1;
    }
  }
  return;
}

void print_pressure(float pressure) {
  char pressure_str[5];
  float mant;
  int8_t exp;
  float_to_exp(pressure, &mant, &exp);

  // Serial.print("pressure: ");
  // Serial.println(pressure);
  // Serial.print("mant:     ");
  // Serial.println(mant);
  // Serial.print("exp:      ");
  // Serial.println(exp);
  // Serial.println();

  dtostrf(fabs(mant), 1, 2, pressure_str);
  char *byte0 = mant<0 ? "-" : " ";
  char byte1[3] = "0.";
  byte1[0] = pressure_str[0];
  char byte2[2];
  byte2[0] = pressure_str[2];
  char byte3[2];
  byte3[0] = pressure_str[3];
  char byte4[2] = " ";
  char byte5[2] = "E";
  char *byte6 = exp<0 ? "-" : " ";
  char byte7[3] = "0";
  sprintf(byte7, "%d", abs(exp));

  if(abs(exp) < 10) {
    LED.print(byte0, byte1, byte2, byte3, byte4, byte5, byte6, byte7);
  } else {
    LED.print(byte0, byte1, byte2, byte3, byte5, byte6, byte7, &byte7[1]);
  }
}

void setup() {
  pinMode(SWROT1, INPUT_PULLUP);
  pinMode(SWROT2, INPUT_PULLUP);
  pinMode(SWROT3, INPUT_PULLUP);
  pinMode(SWROT4, INPUT_PULLUP);
  pinMode(SWROT5, INPUT_PULLUP);
  pinMode(SWROT6, INPUT_PULLUP);
  pinMode(PRSSR, INPUT);

  LED.begin(LED.e8Bit);
  LED.setDisplayArea(1,2,3,4,5,6,7,8);

  Serial.begin(115200);

  Serial.println("Hello stranger! \n\rI am the arduino sitting in a box on the desk in front of you. \n\rIf you want to know more about me and my peripherals, visit my GitLab: https://gitlab.kit.edu/uqfzb/pressure-sensor-readout-unit \n\r\n\rSee you next time :)");
}

void loop() {
  print_pressure(read_pressure(rotsw_to_unit(), 100, 1));
  //LED.print(read_pressure(rotsw_to_unit()));
  if(analogRead(PRSSR) == 1023) {
    LED.print("H", "1");
  }
  delay(100);
}
